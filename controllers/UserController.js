const hashPassword = require('../utils/hash')
const _ = require('lodash')
const express = require('express');
const {
    user,
    validate
} = require('../model/user.model')
var router = express.Router();

//get all users
router.get('/', (req, res) => {
    user.find()
        .then(users => res.send(users))
        .catch(err => res.send(err).status(404));
});

//get All admins
router.get('/admins', (req, res) => {
    user.find({
            isAdmin: "true"
        })
        .then(users => res.status(200).send(users))
        .catch(err => res.status(400).send(err))
})

//get All non-admins
router.get('/notAdmins', (req, res) => {
    user.find({
            isAdmin: "false"
        })
        .then(users => res.status(200).send(users))
        .catch(err => res.status(400).send(err))
})

//sign up
router.post('/', async(req, res) => {
    const {
        error
    } = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)

    let user = await user.findOne({
        email: req.body.email
    })
    if (user) return res.send('user is already registered').status(400)
    user = new user(_.pick(req.body, ['name', 'email', 'password', 'country_id', 'isAdmin']))
    const hashed = await hashPassword(user.password)
    user.password = hashed
    await user.save()
    return res.send(_.pick(user, ['_id', 'name', 'email', 'password', 'country_id', 'isAdmin'])).status(201)
});

module.exports = router;