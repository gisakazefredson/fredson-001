const admin = require('../middlewares/isAdmin')
const express = require('express');
const mongoose = require('mongoose')
const district = mongoose.model('district')
const _ = require("lodash")
const province = mongoose.model('province')

var router = express.Router();
   
router.get('/', (req, res) => {
    district.find()
        .then(district => res.status(200).send(district))
        .catch(err => res.status(400).send(err.message))
})

//get district by id
router.get('/:id', (req, res) => {
    district.find({
            _id: req.params.id
        })
        .then(district => res.status(200).send(district))
        .catch(err => res.status(400).send(err.message))
})

//delete district
router.delete('/:id', admin, (req, res) => {
    district.findByIdAndDelete({
            _id: req.params.id
        })
        .then(district => res.status(200).send(district))
        .catch(err => res.status(400).send(err.message))
})

//get all by the province id
router.get('/byprovinceid/:id', (req, res) => {
    district.find({
            province_id: req.params.id
        })
        .then(district => res.status(200).send(district))
        .catch(err => res.status(400).send(err.message))
})

//insert new record
router.post('/', admin, (req, res) => {
    let province = province.find({
        province_id: req.body.province_id
    })
    if (province == null) {
        return res.status(400).send("the province not found!")
    } else {
        let district = new district();
        district.name = req.body.district_name;
        district.gender = req.body.district_id;
        district.province_id = req.body.province_id;
        district.save()
            .then(district => res.status(200).send(_.pick(district, ["district_name", "district_id"])))
            .catch(err => res.status(400).send(err.message))
    }


});

//update
router.put('/:id', admin, (req, res) => {
    district.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            })
        .then(district => res.status(200).send(district))
        .catch(err => res.status(400).send(err.message))
});

module.exports = router