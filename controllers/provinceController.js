const admin = require('../middlewares/isAdmin')
const express = require('express');
const mongoose = require('mongoose')
const province = mongoose.model('province')
const _ = require("lodash")
const country = mongoose.model('country')

var router = express.Router();
   
router.get('/', (req, res) => {
    province.find()
        .then(province => res.status(200).send(province))
        .catch(err => res.status(400).send(err.message))
})

//get province by id
router.get('/:id', (req, res) => {
    province.find({
            _id: req.params.id
        })
        .then(province => res.status(200).send(province))
        .catch(err => res.status(400).send(err.message))
})

//delete province
router.delete('/:id', admin, (req, res) => {
    province.findByIdAndDelete({
            _id: req.params.id
        })
        .then(province => res.status(200).send(province))
        .catch(err => res.status(400).send(err.message))
})

//get all by the country id
router.get('/bycountryid/:id', (req, res) => {
    province.find({
            country_id: req.params.id
        })
        .then(province => res.status(200).send(province))
        .catch(err => res.status(400).send(err.message))
})

//insert new record
router.post('/', admin, (req, res) => {
    let country = country.find({
        _id: req.body.country_id
    })
    if (country == null) {
        return res.status(400).send("the country not found!")
    } else {
        let province = new province();
        province.name = req.body.province_name;
        province.gender = req.body.province_id;
        province.country_id = req.body.country_id;
        province.save()
            .then(province => res.status(200).send(_.pick(province, ["province_name", "province_id"])))
            .catch(err => res.status(400).send(err.message))
    }


});

//update
router.put('/:id', admin, (req, res) => {
    province.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            })
        .then(province => res.status(200).send(province))
        .catch(err => res.status(400).send(err.message))
});

module.exports = router