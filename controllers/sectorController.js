const admin = require('../middlewares/isAdmin')
const express = require('express');
const mongoose = require('mongoose')
const sector = mongoose.model('sector')
const _ = require("lodash")
const district = mongoose.model('district')

var router = express.Router();
   
router.get('/', (req, res) => {
    sector.find()
        .then(sector => res.status(200).send(sector))
        .catch(err => res.status(400).send(err.message))
})

//get sector by id
router.get('/:id', (req, res) => {
    sector.find({
            _id: req.params.id
        })
        .then(sector => res.status(200).send(sector))
        .catch(err => res.status(400).send(err.message))
})

//delete sector
router.delete('/:id', admin, (req, res) => {
    sector.findByIdAndDelete({
            _id: req.params.id
        })
        .then(sector => res.status(200).send(sector))
        .catch(err => res.status(400).send(err.message))
})

//get all by the district id
router.get('/bydistrictid/:id', (req, res) => {
    sector.find({
            district_id: req.params.id
        })
        .then(sector => res.status(200).send(sector))
        .catch(err => res.status(400).send(err.message))
})

//insert new record
router.post('/', admin, (req, res) => {
    let district = district.find({
        _id: req.body.district_id
    })
    if (district == null) {
        return res.status(400).send("the district not found!")
    } else {
        let sector = new sector();
        sector.name = req.body.sector_name;
        sector.gender = req.body.sector_id;
        sector.district_id = req.body.district_id;
        sector.save()
            .then(sector => res.status(200).send(_.pick(sector, ["sector_name", "sector_id"])))
            .catch(err => res.status(400).send(err.message))
    }


});

//update
router.put('/:id', admin, (req, res) => {
    sector.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            })
        .then(sector => res.status(200).send(sector))
        .catch(err => res.status(400).send(err.message))
});

module.exports = router