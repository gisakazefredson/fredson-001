const admin = require('../middlewares/isAdmin')
const express = require('express');
const mongoose = require('mongoose')
const country = mongoose.model('country')
const _ = require('lodash')
    //Creating a Router
var router = express.Router();

//get all countrys
router.get('/', (req, res) => {
    country.find()
        .then(countrys => res.status(200).send(countrys))
        .catch(err => res.status(400).send(err))
})

//get country by id
router.get('/:id', (req, res) => {
    country.findOne({
            _id: req.params.id
        })
        .then(country => res.status(200).send(_.pick(country, ["_id", "country_name", "continent", "country_id"])))
        .catch(err => res.status(400).send(err.message))
})

//delete country
router.delete('/:id', admin, (req, res) => {
    country.findByIdAndDelete({
            _id: req.params.id
        })
        .then(country => res.status(200).send(_.pick(country, ["country_name", "continent", "country_id"])))
        .catch(err => res.status(400).send(err.message))
})

//insert new record
router.post('/', admin, async(req, res) => {
    let ct = await country.findOne({
        country_id: req.body.country_id
    })
    if (ct) return res.status(400).send("The country is already registered")
    let country = new country();
    country.country_name = req.body.country_name;
    country.continent = req.body.continent;
    country.country_id = req.body.country_id;
    country.save()
        .then(country => res.send(country).status(201))
        .catch(err => res.send(err.message).status(400));

});

//update
router.put('/:id', admin, (req, res) => {
    country.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            })
        .then(countrys => res.status(200).send(countrys))
        .catch(err => res.status(400).send(err.message))
});

module.exports = router