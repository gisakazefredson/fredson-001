//Import the dependencies
const hashPassword = require('../utils/hash')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi')
const _ = require('lodash')
const config = require('config')
const express = require('express');
const {
    user
} = require('../model/user.model')
//Creating a Router
var router = express.Router();

router.post('/jwt', async(req, res) => {
    const {
        error
    } = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)

    let user = await user.findOne({
        email: req.body.email
    })
    if (!user) return res.send('Invalid email or password').status(400)

    if (req.body.password != user.password) return res.send('Invalid email or password').status(400)
    return res.send(user.generateAuthToken())
});

function validate(req) {
    const schema = {
        email: Joi.string().max(255).min(3).required().email(),
        password: Joi.string().max(255).min(3).required()
    }
    return Joi.validate(req, schema)
}
module.exports = router;