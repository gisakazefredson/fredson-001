require('./model/mongodb')
const sectorController = require('./controllers/sectorController')
const districtController = require('./controllers/districtController')
const provinceController = require('./controllers/provinceController')
const countryController = require('./controllers/countryController')
const userController = require('./controllers/userController');
const auth = require('./controllers/auth')
const config = require('config')
const authMiddleware = require('./middlewares/auth')
    //Import the necessary packages
const express = require('express');

var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());

if (!config.get("jwtPrivateKey")) {
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
}

app.get('/', (req, res) => {
    res.send('Welcome to Administrative System');
});

app.use('/api/sectors', authMiddleware, sectorController)
app.use('/api/districts', authMiddleware, districtController)
app.use('/api/provinces', authMiddleware, provinceController)
app.use('/api/countries', authMiddleware, countryController)
app.use('/api/users', userController)
app.use('/api/auth', auth)

//Establish the server connection
const port = process.env.PORT || 3500;
app.listen(port, () => console.log(`Listening on port ${port}..`));