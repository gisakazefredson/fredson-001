const mongoose = require('mongoose');

var provinceSchema = new mongoose.Schema({
    province_name: {
        type: String,
        required: 'this field is required!',
        minlength: 5,
        maxlength: 250
    },
    province_id: {
        type: String,
        required: true
    },
    country_id: {
        type: String,
        required: true
    }
});

mongoose.model('province', provinceSchema);