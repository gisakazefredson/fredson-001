const mongoose = require('mongoose');

var sectorSchema = new mongoose.Schema({
    sector_name: {
        type: String,
        required: 'this field is required!',
        minlength: 5,
        maxlength: 250
    },
    district_id: {
        type: String,
        required: true
    },
    country_id: {
        type: String,
        required: true
    }
});

mongoose.model('sector', sectorSchema);