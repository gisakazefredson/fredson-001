const mongoose = require('mongoose');

var districtSchema = new mongoose.Schema({
    district_name: {
        type: String,
        required: 'this field is required!',
        minlength: 5,
        maxlength: 250
    },
    district_id: {
        type: String,
        required: true
    },
    province_id: {
        type: String,
        required: true
    }
});

mongoose.model('district', districtSchema);