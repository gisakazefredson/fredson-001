const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/Administration_Rwanda', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('connected to mongodb successfully....'))
    .catch(err => console.log('failed to connect to mongodb', err));

//Connecting Node and MongoDB
require('./user.model');
require('./sector.model');
require('./district.model');
require('./province.model');
require('./country.model');