const mongoose = require('mongoose');
var countrySchema = new mongoose.Schema({
    country_name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 300
    },
    continent: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 200
    },
    country_id: {
        type: String,
        required: true,
    }
});
mongoose.model("country", countrySchema)